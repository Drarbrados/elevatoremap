package elevator.model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by User on 16.11.2014.
 */
public abstract class Passenger{
    private static AtomicInteger nextID = new AtomicInteger(0);
    
    private final int id;
    private volatile Store destinationStore;
    private volatile Location location;
    protected volatile TransportationState state = TransportationState.NOT_STARTED;

    public Passenger() {
        id = nextID.getAndIncrement();
    }
    
    public void setDestinationStory(Store store) {
    	if(store == null)
    		throw new NullPointerException("Null-pointer to store.");
    	destinationStore = store;
    }
    public Store getDestinationStory() {
        return destinationStore;
    }

    public abstract void run( );

    public void setLocation(Location location) {
    	if(location == null)
    		throw new NullPointerException("Null-pointer to location.");
    	this.location = location;
    }
    public Location getLocation() {
        return location;
    }

    public TransportationState getState() {
    	return state;
    }

    public int getID( ){
    	return id;
    }

    public int hashCode(){
    	return id;
    }

    public String toString( ){
        return "Passenger_" + id + " in " + location;

    }
    
    public boolean isGoodDestination(Store store){
    	return destinationStore.equals(store);
    }
    
}
