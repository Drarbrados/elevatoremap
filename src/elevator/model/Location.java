package elevator.model;

public interface Location {
	public Building getBuilding( );
	public void setBuilding(Building buiding);
	
	
	public void waitWithLog() throws InterruptedException;
	public void notifyAllAndWait();
	public void countDown();
	
	public void acquire();
	public void relise();
}
