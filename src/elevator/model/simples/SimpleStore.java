package elevator.model.simples;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;

import elevator.model.AbstractLocation;
import elevator.model.Building;
import elevator.model.Passenger;
import elevator.model.Store;
import elevator.model.exceptions.PassengerIsNotInThisLocationException;

/**
 * Created by User on 16.11.2014.
 */
public class SimpleStore extends AbstractLocation implements Store{
	private static final Logger log = Logger.getLogger(SimpleStore.class);
	
    private volatile ConcurrentSkipListSet<Passenger> dispatchSet
            = new ConcurrentSkipListSet<Passenger>(
                	new Comparator<Passenger>() {
            	        @Override
            	        public int compare(Passenger p1, Passenger p2) {
            	            return p1.getID() - p2.getID();
                    }
            });
    private volatile CopyOnWriteArrayList<Passenger> arrivalList
            = new CopyOnWriteArrayList<Passenger>();
    
    private volatile int numb = -1;
    
    public SimpleStore(Building building){
    	setBuilding(building);
    }

    public void addDispatchPassenger(Passenger p){
    	if(p == null)
    		throw new NullPointerException("Null-pointer to passenger.");
    		dispatchSet.add(p);
    //		log.debug(p + " add to dispatch container.");
    }

    public void removeDispatchPassenger(Passenger p){
    	if(!dispatchSet.remove(p)){
			String err = "Passenger try to exit from dispatch " + this
    				+ " but he placed in " + p.getLocation() + ".";
    		log.warn(err);
    		throw new PassengerIsNotInThisLocationException(err);
    	}
    	
  //  	log.debug(p.toString() + " removed from dispatch container.");
    }

    public void addArrivalPassenger(Passenger p){
    	arrivalList.add(p);
   // 	log.debug(p + " add to arival container.");
    }
    
    public void removeArrivalPassenger(Passenger p){
    	if(!arrivalList.remove(p)){
			String err = "Passenger try to exit from " + this
    				+ " but he placed in " + p.getLocation() + ".";
    		log.warn(err);
    		throw new PassengerIsNotInThisLocationException(err);
    	}
 //   	log.debug(p + " add to arival container.");
    }

    public int getDispatchNumb() {
        return dispatchSet.size();
    }
    
    public int getArrivalNumb() {
        return arrivalList.size();
    }
    
    public int getAllPassengersNumb() {
        return arrivalList.size() + dispatchSet.size();
    }
    
    public int getStoreNumber(){
		return numb;
	}
    
	public void setStoreNumber(int numb) {
		this.numb = numb;
	}

	public LinkedList<Passenger> getAllPassengers() {
    	LinkedList<Passenger> list = new LinkedList<Passenger>();
    	for(Passenger p : arrivalList)
    		list.add(p);
    	
    	for(Passenger p : dispatchSet)
    		list.add(p);;
        return list;
    }


    @Override
    public boolean equals(Object other){
        return this == other;
    }
    @Override
    public String toString(){ return "Store" + numb; }
}
