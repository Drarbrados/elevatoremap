package elevator.model.simples;

import elevator.model.AbstractLocation;
import elevator.model.Controller;
import elevator.model.Elevator;
import elevator.model.Passenger;
import elevator.model.exceptions.ElevatorIsFullException;
import elevator.model.exceptions.PassengerIsNotInThisLocationException;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;

/**
 * Created by User on 18.11.2014.
 */
public class SimpleElevator extends AbstractLocation implements Elevator{
	private static final Logger log = Logger.getLogger(SimpleElevator.class);

    private volatile Controller controller;
	private volatile int capacity;
    private final TreeSet<Passenger> elevatorContainer
	= new TreeSet<Passenger>(
    	new Comparator<Passenger>() {
	        @Override
	        public int compare(Passenger p1, Passenger p2) {
	            return p1.getID() - p2.getID();
        }
    });
    private final Semaphore mutex = new Semaphore(1);
    
    public void board(Passenger p) throws ElevatorIsFullException{
    	try {
			mutex.acquire();
	    	if(elevatorContainer.size() >= capacity)
	    	//	log.info("No more space in elevator.");
	    		throw new ElevatorIsFullException(
	    				"No more space in elevator.");
	        elevatorContainer.add(p);
		} catch (InterruptedException e) {
			log.warn("Thread " + Thread.currentThread()
					+ " was interrupted in Elevator.board().");
		}
    	finally{
            mutex.release();
    	}
        
    }
    
	public void deboard(Passenger p){
    	try {
			mutex.acquire();
	    	
			if (!elevatorContainer.remove(p)){
				String err = "Passenger try to exit from " + this
	    				+ " but he placed in " + p.getLocation() + ".";
	    		log.warn(err);
	    		throw new PassengerIsNotInThisLocationException(err);
	    	}
	    	
		} catch (InterruptedException e) {
			log.warn("Thread " + Thread.currentThread()
					+ " was interrupted in Elevator.deboard().");
		}
    	finally{
    		mutex.release();
    	}
	}


    public Controller getController() {
        return controller;
    }

    public void setController(Controller controller) {
    	if(controller == null)
    		throw new NullPointerException("Null-pointer to controller.");
        this.controller = controller;
    }
    
    public int getSize( ){
    	int answ = -1;
    	try {
			mutex.acquire();
	    	
			answ = elevatorContainer.size();
		} catch (InterruptedException e) {
			log.warn("Thread " + Thread.currentThread()
					+ " was interrupted in Elevator.getSize().");
		}
    	finally{
    		mutex.release();
    	}
    	
    	return answ;
    }
    
    public void setCapacity(int capacity) {
        //	if(elevatorContainer.size() > capacity)
    	  //  		throw new 
    	try {
			mutex.acquire();
	    	this.capacity = capacity;
		} catch (InterruptedException e) {
			log.warn("Thread " + Thread.currentThread()
					+ " was interrupted in Elevator.setCapacity().");
		}
    	finally{
    		mutex.release();
    	}
		
	}

	public int getCapacity() {
		return capacity;
	}
    
    public LinkedList<Passenger> getAllPassengers() {
    	LinkedList<Passenger> list = new LinkedList<Passenger>();
    	try {
			mutex.acquire();
	    	
			for(Passenger p : elevatorContainer)
	    		list.add(p);
		} catch (InterruptedException e) {
			log.warn("Thread " + Thread.currentThread()
					+ " was interrupted in Elevator.getAllPassengers().");
		}
    	finally{
    		mutex.release();
    	}
    	
        return list;
    }
    
    public String toString(){ return "Elevator"; }
}
