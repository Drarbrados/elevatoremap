package elevator.model.simples;

import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import elevator.model.Building;
import elevator.model.Controller;
import elevator.model.Elevator;
import elevator.model.Location;
import elevator.model.Passenger;
import elevator.model.Store;
import elevator.model.exceptions.ElevatorIsFullException;
import elevator.model.exceptions.PassengerIsNotInThisLocationException;

/**
 * Created by User on 18.11.2014.
 */
public class SimpleController implements Controller{
	private static final Logger log = Logger.getLogger(SimpleController.class);
    private volatile Elevator elevator;
    private volatile AtomicBoolean up = new AtomicBoolean(true);
    private final boolean consoleAlternative;
    
    public SimpleController( ){
    	consoleAlternative = Boolean.valueOf(ResourceBundle.getBundle("config")
    			.getString("consoleAlternative"));
    	if(consoleAlternative)
    		Logger.getRootLogger().removeAppender("clogger");
    }
    
    @Override 
	public void doBoardElevator(Passenger p) {
    	Location loc;
    	(loc = p.getLocation()).acquire();
    	boolean loop = true;
    	while(loop)
	    	try {
		   		try {
					loc.waitWithLog();
				} catch (InterruptedException ie) {
				}
	    		
	    		loop = !tryBoardElevator(p);
				if(!loop){
					StringBuilder sb = new StringBuilder("BOARDING_OF_PASSENGER (");
					sb.append(p.getID());
					sb.append(" on ");
					sb.append(loc.getBuilding().getCurrentStoreNumb());
					sb.append(')');
					log.info(sb);
				}
			} catch (ElevatorIsFullException e) {
			//	log.info(p + " is late for boarding, elevator is full.");
			} catch (PassengerIsNotInThisLocationException e) {
				log.warn(p + " is not in good store to bodard.");
				log.warn(e.getLocalizedMessage());
			} finally{
    			loc.countDown();
    		}
    	loc.relise();
    }
    
	@Override
    public boolean tryBoardElevator(Passenger p) throws
    			ElevatorIsFullException {
    	p.getLocation();
		Store bs = elevator.getBuilding().getCurrentStore();
    	if(!p.getLocation().equals(bs))
    		throw new PassengerIsNotInThisLocationException(
    				"Bad loaction for boarding." + p.getLocation() + " ! " + bs);

    	int curr = bs.getStoreNumber(), all = elevator.getBuilding()
    			.getStoriesNumber();
    	int dir = p.getDestinationStory().getStoreNumber() - curr;
    	boolean isup = up.get();
    	if(dir >= 0  && isup 
    			|| dir <= 0 && !isup
    			|| dir % (all-1) == 0){// if some bug with up.get()
	        elevator.board(p);
		    elevator.getBuilding().getCurrentStore().removeDispatchPassenger(p);
		    p.setLocation(elevator);
    	}
    	else
    		return false;
    	return true;
    }
	
	
	
    @Override 
	public void doDeboardElevator(Passenger p) {
    	Location loc;
    	(loc = p.getLocation()).acquire();
    	boolean loop = true;
    	while(loop)
	    	try {
		   		try {
					loc.waitWithLog();
				} catch (InterruptedException ie) {
				}
		   		
	    		loop = !tryDeboardElevator(p);
	    		if(!loop){
					StringBuilder sb = new StringBuilder("DEBOARDING_OF_PASSENGER (");
					sb.append(p.getID());
					sb.append(" on ");
					sb.append(loc.getBuilding().getCurrentStoreNumb());
					sb.append(')');
					log.info(sb);
				}
			} catch (PassengerIsNotInThisLocationException e) {
				log.warn(p + " is not in elevator to deboard.");
			} finally{
    			loc.countDown();
    		}
    	loc.relise();
    }
    
	@Override
    public boolean tryDeboardElevator(Passenger p) {
        Building b = elevator.getBuilding();
        if(p.isGoodDestination(b.getCurrentStore())){
        	b.getCurrentStore().addArrivalPassenger(p);
	        elevator.deboard(p);
	        p.setLocation(b.getCurrentStore());
        }
        else
        	return false;
        return true;
    }
	
	
    
    @Override
    public void run() {
        log.info("STARTING_TRANSPORTATION");
        long iter = 0L;
        Building b = null;
    	b = elevator.getBuilding();
        int wait;
        do {
      //  	log.info("DEBOARDING on " + b.getCurrentStoreNumb() + ":");
            
        	b.doDeboarding();
      //      log.info("BOARDING on " + b.getCurrentStoreNumb() + ":");
           
            b.doBoarding();
            if (up.get())
                b.elevatorUp();
            else
                b.elevatorDown();
            if (b.getCurrentStoreNumb() % (b.getStoriesNumber() - 1) == 0)
                up.logicNot();
            
            wait = b.getWaitPassengersNumb();
            if(consoleAlternative){
            	System.out.print("\r Passengers: wait=");
            	System.out.print(wait);
            	System.out.print(" end=");
            	System.out.print(b.getArrivalPassengersNumb());
            	System.out.print("   Elevator: ");
            	System.out.print(elevator.getSize());
            	System.out.print('/');
            	System.out.print(elevator.getCapacity());
            	System.out.print("   Store: ");
            	System.out.print(b.getCurrentStoreNumb());
            	System.out.print(up.get() ? "\tU" : "\tD" );
            	System.out.print("\t\t");
            }
            ++iter;
        } while(wait != 0);
        if(consoleAlternative)
        	System.out.println("\n");
        log.info("COMPLITION_TRANSPORTATION");
        log.debug("\ttotal iterations: " + iter + "\n");
    }
    
	@Override
	public void setElevator(Elevator elevator) {
    	if(elevator == null)
    		throw new NullPointerException("Null-pointer to elevator.");
		this.elevator = elevator;
		
	}
	
	@Override
	public Elevator getElevator() {
		return elevator;
	}
    
    
	
	private class AtomicBoolean{
	    private volatile AtomicInteger i = new AtomicInteger(0);
	    
	    public AtomicBoolean( ){
	    }
	    public AtomicBoolean(boolean v){
	    	this();
	    	if(v)
	    		i.incrementAndGet();
	    }
	    
	    public void logicNot(){
	    	i.incrementAndGet();
	    }
	    
	    public boolean get( ){
	    	return (i.intValue()&1) == 1 ? true : false;
	    }
	} 

}
