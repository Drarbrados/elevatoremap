package elevator.model.simples;

//import org.apache.log4j.Logger;

import elevator.model.Passenger;
import elevator.model.TransportationState;

public class SimplePassenger extends Passenger {
//	private static final Logger log = Logger.getLogger(SimplePassenger.class);
	public SimplePassenger() {
		
	}

	@Override
	public void run(){
		TranportationTask th = new TranportationTask();
        th.start();
		while( th.getState() != Thread.State.WAITING
				&& th.getState() != Thread.State.TERMINATED
				&& th.getState() != Thread.State.TIMED_WAITING)
			Thread.yield();
    }
	
    
    public class TranportationTask extends Thread{
        public void run( ){
            while(state != TransportationState.COMPLETED
            		&& state != TransportationState.ABORTED) {
            //	log.info(SimplePassenger.this + " state is " + state);
	            
            	state = state.doAction(SimplePassenger.this, getLocation().getBuilding()
	                		.getElevator().getController());
            }
            //log.info("\t" + SimplePassenger.this + " END  like " + state);
        }
        
        public String toString( ){
        	return SimplePassenger.this.toString();
        }
    }
}
