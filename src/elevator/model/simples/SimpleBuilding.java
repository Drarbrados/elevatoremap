package elevator.model.simples;

import elevator.model.Building;
import elevator.model.Controller;
import elevator.model.Elevator;
import elevator.model.Passenger;
import elevator.model.Store;
import elevator.model.TransportationState;
import elevator.model.exceptions.UnacceptableStoreExceprion;

import java.awt.Toolkit;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

/**
 * Created by User on 16.11.2014.
 */
public class SimpleBuilding implements Building{
	private static final Logger log = Logger.getLogger(SimpleBuilding.class); 
	
    private Elevator elevator;
    private AtomicInteger currStore = new AtomicInteger(0);
    private Store[] stories;
    

    private final int storiesNumber;
    private final int passengersNumber;
    private final int elevatorCapacity;
    
    {
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        storiesNumber = Integer.valueOf(bundle.getString("storiesNumber"));
        passengersNumber = Integer.valueOf(bundle.getString("passengersNumber"));
        elevatorCapacity = Integer.valueOf(bundle.getString("elevatorCapacity"));
        stories = new Store[storiesNumber];
    }
    
    public SimpleBuilding(){
    	log.debug("initing_building");
    	elevator = new SimpleElevator();
    	Controller controller = new SimpleController();
    	elevator.setController(controller);
    	controller.setElevator(elevator);
    	elevator.setBuilding(this);
    	elevator.setCapacity(elevatorCapacity);
    	
        for(int i = 0; i < stories.length; ++i)
            stories[i] = new SimpleStore(this);
        
        init();
    }
    
	private void init(){
        for(int i = 0; i < stories.length; ++i)
            stories[i].setStoreNumber(i);
        
        int from, to;
        Passenger p;
        Random rand = new Random();
 /*       
        class CallablePassengerRunner implements Callable<Passenger>{
        	Passenger pass;
        	
        	CallablePassengerRunner(Passenger p){ pass = p; }
			@Override
			public Passenger call() throws Exception {
				pass.run();
				return pass;
			}
        }
        
        ArrayList<CallablePassengerRunner> ps =
        		new ArrayList<CallablePassengerRunner>(passengersNumber);*/
        for(int i = 0; i < passengersNumber; ++i){
        	from = rand.nextInt(stories.length);
        	to = rand.nextInt(stories.length - 1);
            if(to >= from)
                ++to;
            stories[from].setBuilding(this);
            stories[to].setBuilding(this);
            
            p = new SimplePassenger();
            p.setLocation(stories[from]);
            p.setDestinationStory(stories[to]);
            stories[from].addDispatchPassenger(p);
            
            p.run();
            //// move me to this.run();
           // ps.add(new CallablePassengerRunner(p) );
        }
/*        if(stories.length < 16 ){
	        StringBuffer sb = new StringBuffer("Passenger on stories: ");
	        for(int i = 0; i < stories.length; ++i){
	        	sb.append("\n\t ");
	        	sb.append(i);
	        	sb.append(":");
	        	sb.append(stories[i].getDispatchNumb());
	        }
	        log.info(sb);
        }*/
    //    System.out.println("  Start excecuting passengers");
/*    	ExecutorService exc = null;
        try {
        	if(passengersNumber > 54){
	//        	System.out.println((int) ((4*passengersNumber+0.5)/Math.log(passengersNumber)));
	        	exc = Executors.newFixedThreadPool((int) ((passengersNumber+0.5)
	        			/Math.log(passengersNumber)));
        	}
        	else{
//	        	System.out.println(passengersNumber);
	        	exc = Executors.newFixedThreadPool(passengersNumber);
        	}

        	exc.invokeAll(ps);
		} catch (InterruptedException e) {
			log.error("Thread-invoker was interrupted. Not all passenger was started.");
	   //     System.out.println("  EROR of excecuting");
		}
        finally{
        	exc.shutdown();
     //       System.out.println("  Excute complite");
        }*/
    	log.debug("initing_complite.");
    }
    
	@Override
    public void run() {
        elevator.getController().run();
    }

    
	
	@Override
    public int getStoriesNumber(){ return stories.length; }


	
	@Override
    public void elevatorUp(){
		int prev;
//        synchronized (currStore) {
//            if (currStore.intValue() == stories.length - 1) {
//                throw new UnacceptableStoreExceprion("Can't move up on the highest floor.");
//            }
            prev = currStore.getAndIncrement();
//        }
        
            StringBuilder sb = new StringBuilder("MOVING_ELEVATOR( from ");
            sb.append(prev);
            sb.append(" to ");
            sb.append(currStore);
            sb.append(")");
     /*       if(elevator.getSize() <= 10 && elevator.getSize() != 0){
	            sb.append("\n\tare:");
	            for(Passenger p : elevator.getAllPassengers()){
	        		sb.append(' ');
	        		sb.append(p.getID());
	            }
            }*/
            //System.out.println(sb);
            log.info(sb);
    }
    
	
	@Override
    public void elevatorDown(){
//        synchronized (currStore) {
//            if (currStore.intValue() == 0) {
//               throw new UnacceptableStoreExceprion("Can't move down on the lowest floor.");
//            }
            int prev = currStore.decrementAndGet();
//       }
            StringBuilder sb = new StringBuilder("MOVING_ELEVATOR( form ");
            sb.append(prev);
            sb.append(" to ");
            sb.append(currStore);
            sb.append(")");
/*            if(elevator.getSize() <= 10 && elevator.getSize() != 0){
	            sb.append("\n\tare:");
	            for(Passenger p : elevator.getAllPassengers()){
	        		sb.append(' ');
	        		sb.append(p.getID());
	            }
            }*/
            
           //System.out.println(sb);
            log.info(sb);
    }
	@Override
    public Store getCurrentStore( ){    return stories[currStore.intValue()];  }
	@Override
	public int getCurrentStoreNumb( ){    return currStore.intValue();  }
	
	@Override
    public void doDeboarding( ){
		elevator.acquire();
		elevator.notifyAllAndWait();
		elevator.relise();
    }

	@Override
    public void doBoarding( ){
		Store loc = stories[currStore.intValue()];
		loc.acquire();
		loc.notifyAllAndWait();
		loc.relise();
    }
	@Override
    public int getArrivalPassengersNumb( ){
        int sum = 0;
        for (Store st : stories)
        	sum += st.getArrivalNumb();
        return sum;
    }
    
	@Override
    public int getWaitPassengersNumb( ){
        int sum = elevator.getSize();
        for (Store st : stories)
        	sum += st.getDispatchNumb();
        return sum;
    }
    
	@Override
    public int getWaitPassengersNumb(int store) {
        if(store < 0 || store >= stories.length)
   		throw new UnacceptableStoreExceprion("Can't get nonexistent floor.");
        return stories[store].getDispatchNumb();
    }
    
	@Override
    public void setElevator(Elevator elevator) {
    	if(elevator == null)
    		throw new NullPointerException("Null-pointer to elevator.");
        this.elevator = elevator;
    }

	@Override
    public Elevator getElevator() {
        return elevator;
    }

	@Override
	public Store getStore(int numb) {
		if(numb < 0 || numb > stories.length){
			throw new UnacceptableStoreExceprion("Wrong store number.");
		}
		return stories[numb];
	}
	
	public boolean validate() {
		Toolkit.getDefaultToolkit().beep();
		log.debug("Validation.");
		int validate = 0;
		if(elevator.getSize() != 0){
			System.out.println("Elevator isn't empty.");
			log.info("Elevator isn't empty.");
			++validate;

		/*	System.out.println(".checking elevator");
			for(Passenger p : elevator.getAllPassengers()){
				
			}*/
		}
		String temp;
		int sum = 0;
		LinkedList<Passenger> list = new LinkedList<Passenger>(), tmp;
		for(int i = 0; i < stories.length; ++i){
			//System.out.println(".checking " + i + " store");
			if(stories[i].getDispatchNumb() != 0){
				temp = "Dispatches in " + i + " store.";
				System.out.println(temp);
				log.error(temp);
				++validate;
			}
			sum += stories[i].getArrivalNumb();
			
			tmp = stories[i].getAllPassengers();
			list.addAll(tmp);
			for(Passenger p : tmp){
				if(p.getState() != TransportationState.COMPLETED){
					temp = p + " is in "
							+ p.getState() + " state.";
					System.out.println(temp);
				//	if(p.getState() == TransportationState.ABORTED)
				//		log.warn(temp);
			//		else
						log.error(temp);
					++validate;
				}
				if(p.getDestinationStory() != stories[i]){
					temp = p + " has wrond destination story. Expected "
							+ p.getDestinationStory().getStoreNumber() + " but found " + i + ".";
					System.out.println(temp);
					log.error(temp);
					++validate;
				}
			}
		}
		
		if(sum != passengersNumber){
			temp = "Wrong final passengers number. Excpected " + 
					passengersNumber + ", but found " + sum + ".";
			System.out.println(temp);
			log.error(temp);
			++validate;
			if(list.size() <= 20){
				Collections.sort(list, 	    	new Comparator<Passenger>() {
			        @Override
			        public int compare(Passenger p1, Passenger p2) {
			            return p1.getID() - p2.getID();
			        }}
			        );
				StringBuilder sb = new StringBuilder("They are: ");
				for(Passenger p : list){
					sb.append(p.getID());
					sb.append(" ");
				}
				System.out.println(sb);
				log.error(sb);
			}
		}
		sum = getWaitPassengersNumb();
		
		temp = "Dispatch passengers: " + sum;
		if(sum != 0)
			log.error(temp);
		
		if(validate == 0){
//			System.out.println("\nvalidate: OK.");
			log.info("VALIDATE: OK.");
		}
		else{
			temp = "VALIDATE: ERROR  found "
					+ validate + " errors.";
//			System.out.println(temp);
			log.error(temp);
		}
		
		Toolkit.getDefaultToolkit().beep();
		return validate == 0;
	}
    
}
