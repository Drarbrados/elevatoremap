package elevator.model;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

//import org.apache.log4j.Logger;


public abstract class AbstractLocation implements Location {
//	private static final Logger log = Logger.getLogger(AbstractLocation.class);
	
	private volatile Building building;
	private final AtomicInteger runnings = new AtomicInteger(0),
			waiters = new AtomicInteger(0);
	private volatile Thread notifier;
	private final ReentrantLock lock = new ReentrantLock();
	private final Condition cond = lock.newCondition();
	
	public AbstractLocation(){
	}
	@Override
	public void acquire( ){
		lock.lock();
	}
	@Override
	public void relise( ){
		lock.unlock();
	}
	
	
    @Override
    public Building getBuilding() {
        return building;
    }
    
    @Override
    public void setBuilding(Building building) {
    	if(building == null)
    		throw new NullPointerException("Null-pointer to building.");
        this.building = building;
    }
    
    
    
	@Override
	public void waitWithLog( ) throws InterruptedException {
		if(!lock.isHeldByCurrentThread())
			throw new IllegalMonitorStateException(// it need
					"Illegal monitor sate to await(). " + this);
		try {
			waiters.incrementAndGet();
			cond.await();
		}
		finally{
			waiters.decrementAndGet();
		}
	}
	@Override
	public void countDown( ){
		if(runnings.decrementAndGet() <= 0){
			notifier.interrupt();
			notifier = null;
			
	//		log.info("Passengers end moving on " +
	//				this + ".");
		}
	}
	
	@Override
	public void notifyAllAndWait() {
		lock.lock();
		runnings.set(waiters.get());
		
		try {
//			log.info("Passengers start moving on " + toString() + ".");
			cond.signalAll();
			notifier = Thread.currentThread();
			if(runnings.get() != 0)
				cond.await();
//			else
//				log.info("No passengers to moving on " +
//						this + ".");
		} catch (InterruptedException e) {
			Thread.currentThread().isInterrupted();
		}
		
		lock.unlock();
	}
}