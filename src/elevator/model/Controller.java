package elevator.model;

import elevator.model.exceptions.ElevatorIsFullException;
import elevator.model.exceptions.PassengerIsNotInThisLocationException;

/**
 * Created by User on 18.11.2014.
 */
public interface Controller {

    public abstract void run();
    public abstract void doBoardElevator(Passenger p);
    /**
     * @throws ElevatorIsFullException, PassengerIsNotInThisLocationException
     * @param p
     * @return
     */
    public abstract boolean tryBoardElevator(Passenger p)
    		throws ElevatorIsFullException;
	
    public void doDeboardElevator(Passenger p);
    /**
     * @throws PassengerIsNotInThisLocationException
     * @param p
     * @return
     */
    public boolean tryDeboardElevator(Passenger p);

    public void setElevator(Elevator elevator);
    public Elevator getElevator( );
}
