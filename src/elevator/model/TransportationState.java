package elevator.model;

import org.apache.log4j.Logger;

/**
 * Created by User on 17.11.2014.
 */
public enum TransportationState {

    NOT_STARTED{
        @Override
        public TransportationState doAction(Passenger p, Controller c) {;
			c.doBoardElevator(p);
			
			if(c.getElevator().equals(p.getLocation()))
				return IN_PROGRESS;
			log.warn(p + " can't be set in IN_PROGRES state, "
					+ "becouse of bad result location.");
			
			if(p.getLocation() instanceof Elevator)
				return IN_PROGRESS;
			else
				return NOT_STARTED;
        }
        
        public String toString( ){ return "NOT_STARTED"; }
    },

    IN_PROGRESS{
        @Override
        public TransportationState doAction(Passenger p, Controller c) {
        	c.doDeboardElevator(p);
			
        	if(p.getDestinationStory().equals(p.getLocation()))
				return COMPLETED;
			log.error(p + " can't be set in COMPLETED state, "
					+ "becouse of bad result location.");
			
			if(p.getLocation() instanceof Elevator)
				return IN_PROGRESS;
			else
				return NOT_STARTED;
        }
        
        public String toString( ){ return "IN_PROGRESS"; }
    },

    COMPLETED{
        @Override
        public TransportationState doAction(Passenger passenger, Controller c){
        	return COMPLETED;
        }
        public String toString( ){ return "COMPLETED"; }
    },

    ABORTED{
        @Override
        public TransportationState doAction(Passenger passenger, Controller c){
        	return ABORTED;
        }
        public String toString( ){ return "ABORTED"; }
    };

    /**
     * Performance & return next state;
     * @param passenger
     * @return next state
     */
    public abstract TransportationState doAction(Passenger passenger,
    		Controller controller);

	protected static final Logger log = Logger.getLogger(TransportationState.class);

}
