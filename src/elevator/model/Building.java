package elevator.model;

public interface Building {
    
    public void run();
    public int getStoriesNumber();

    
    /**
     * @throws UnacceptableStoreExceprion
     */
    public void elevatorUp();
    
    /**
     * @throws UnacceptableStoreExceprion
     */
    public void elevatorDown();

    public Store getCurrentStore( );
	public int getArrivalPassengersNumb();
    public int getCurrentStoreNumb( );
    public Store getStore(int numb);
    public void doDeboarding( );
    public void doBoarding( );

    public int getWaitPassengersNumb( );
    
    /**
     * @throws UnacceptableStoreExceprion
     */
    public int getWaitPassengersNumb(int store);
    public void setElevator(Elevator elevator);
    public Elevator getElevator();
}
