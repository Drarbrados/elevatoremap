package elevator.model;

import java.util.LinkedList;

import elevator.model.exceptions.ElevatorIsFullException;
import elevator.model.exceptions.PassengerIsNotInThisLocationException;

public interface Elevator extends Location {
	   
    public void board(Passenger p) throws ElevatorIsFullException;
    /**
     * @param p
     * @throws PassengerIsNotInThisLocationException
     */
	public void deboard(Passenger p);


    public Controller getController();
    public void setController(Controller controller);
    
    public int getSize( );
	public int getCapacity();
    public void setCapacity(int capacity);
    
    public LinkedList<Passenger> getAllPassengers();
	public Building getBuilding();
}
