package elevator.model;

import java.util.LinkedList;

import elevator.model.exceptions.PassengerIsNotInThisLocationException;

public interface Store extends Location {
    public void addDispatchPassenger(Passenger p);
    /**
     * @param p
     * @throws PassengerIsNotInThisLocationException
     */
    public void removeDispatchPassenger(Passenger p);

    public void addArrivalPassenger(Passenger p);
    /**
     * @param p
     * @throws PassengerIsNotInThisLocationException
     */
    public void removeArrivalPassenger(Passenger p);

    public int getDispatchNumb();
    public int getArrivalNumb();
    public int getAllPassengersNumb();
    
    public int getStoreNumber();
	public void setStoreNumber(int numb);

	public LinkedList<Passenger> getAllPassengers();
}
