package elevator.model.exceptions;

/**
 * Created by User on 18.11.2014.
 */
public class ElevatorIsFullException extends Exception {
	private static final long serialVersionUID = -6112431897599367819L;
	public ElevatorIsFullException(){}
    public ElevatorIsFullException(String str){ super(str); }
}
