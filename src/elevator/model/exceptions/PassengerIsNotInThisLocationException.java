package elevator.model.exceptions;

/**
 * Created by User on 18.11.2014.
 */
public class PassengerIsNotInThisLocationException extends RuntimeException {
	private static final long serialVersionUID = 331397333644113774L;
	public PassengerIsNotInThisLocationException(){}
    public PassengerIsNotInThisLocationException(String str){ super(str); }
}
